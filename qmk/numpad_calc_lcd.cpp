/* Copyright 2021 Coliss86
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include QMK_KEYBOARD_H
#include <string.h>

#include "calculator.h"
#include "numpad_calc_lcd.h"
#include "SPI_VFD.h"
#include "string_utils.h"
#include "dtostrf.h"

char* ERR_MSG[] = { \
  "OK", \
  "Expr too long",  \
  "Expr empty", \
  "Not sufficient operator arguments", \
  "Insufficient memory", \
  "Division by zero", \
  "Unknown token", \
  "Too many values", \
  "Result too high", \
  "Stack overflow", \
  "Parentheses mismatched"
};

#define DISPLAY_SIZE 20

char input[EXPR_MAX_SIZE] = "";
char display[DISPLAY_SIZE] = "";
char output[RESULT_MAX_SIZE] = "";

void calc_clear(bool cursor) {
  if (cursor) {
    spi_vfd_blink();
    spi_vfd_cursor();
  } else {
    spi_vfd_noBlink();
    spi_vfd_noCursor();
  }
  spi_vfd_clear();
  spi_vfd_home();
  spi_vfd_display();
}

void calc_init(void) {
  spi_vfd_init(VFD_BRIGHTNESS100);
  calc_clear(false);
  spi_vfd_print(" Numpad Calculator");
  wait_ms(500);
  calc_off();
}

void calc_write(char * value) {
  calc_reset_input_and_screen(false);
  spi_vfd_print(value);
}

#define MIN(a, b) (((a) < (b)) ? (a) : (b))

void screen_display(char *value) {
  display[0] = '\0';
  int size = MIN(DISPLAY_SIZE - 1, strlen(value));
  int offset = strlen(value) - DISPLAY_SIZE + 1;
  if (offset < 0) {
    offset = 0;
  }
  for (int i = 0; i < size; i++) {
    display[i] = value[offset + i];
  }
  display[size] = '\0';
  spi_vfd_setCursor(0, 0);
  spi_vfd_print(display);
}

void calc_append(char value) {
  output[0] = '\0';
  if (strlen(input) == 0) {
    calc_clear(true);
  }

  st_append(input, value);
  screen_display(input);
}

void calc_remove_last() {
  st_remove_last(input);
  calc_clear(true);
  screen_display(input);
}

void calc_operator(char value) {
  if (strlen(input) == 0) {
    calc_reset_input_and_screen(true);
    if (strlen(output) > 0) {
      strcat(input, output);
      screen_display(input);
    }
  }

  calc_append(value);
}

void calc_calculate(void) {
  if (strlen(input) == 1) {
    calc_reset_input_and_screen(true);
    if (strlen(output) > 0) {
      strcat(input, output);
      spi_vfd_print(input);
    }
    return;
  }

  double numerical_result=0;
  spi_vfd_noBlink();
  spi_vfd_noCursor();
  output[0] = '\0';
  spi_vfd_setCursor(0,1);

  uint8_t status = calculate(input, numerical_result);
  if (status == 0) {
    double_to_str(numerical_result, output);
    spi_vfd_print(output);
  } else {
    spi_vfd_print(ERR_MSG[status]);
  }
  input[0] = '\0';
}

void calc_off(void) {
  calc_reset_input_and_screen(false);
  spi_vfd_noDisplay();
}

void calc_reset_input_and_screen(bool cursor) {
  input[0] = '\0';
  calc_clear(cursor);
}

char * calc_get_output(void) {
  return output;
}

char * calc_get_input(void) {
  return input;
}

void double_to_str(double result, char *result_string) {
  dtostrf(result, 1, 4, result_string);
  char c;
  for (int i = strlen(result_string)-1; i >= 0; i--) {
    c = result_string[i];
    if (c == '.' || c >= '1' && c <= '9') {
      break;
    } else {
      result_string[i] = '\0';
    }
  }

  if (result_string[strlen(result_string)-1] == '.') {
    result_string[strlen(result_string)-1] = '\0';
  }
}
