/* Copyright 2021 Coliss86
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include QMK_KEYBOARD_H

#include <string.h>
#include "keymap_french.h"
#include "sendstring_french.h"
#include "numpad_calc_lcd.h"

enum layers {
    _NUM = 0,
    _FUNC,
    _CALC,
    _CALC_FUNC
};

enum custom_keycodes {
    FUNC = SAFE_RANGE,
    START_CALC, SEND_RES, CALC_DEL,
    M_RESET, LR_CFUN,
    TIMER_1, TIMER_2, TIMER_3, TIMER_4, TIMER_5, TIMER_6, TIMER_7, TIMER_8, TIMER_9,
};

enum tap_dance {
    TD_NUM,
};

#define LR_FUNC  LT(_FUNC, KC_BSPC)
#define xxxxxxx  KC_NO

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [_NUM] = LAYOUT(
        LR_FUNC, FR_LPRN, FR_RPRN, TD(TD_NUM),
        KC_ESC,  KC_PSLS, KC_PAST, KC_PMNS,
        KC_P7,   KC_P8,   KC_P9,   KC_PPLS,
        KC_P4,   KC_P5,   KC_P6,
        KC_P1,   KC_P2,   KC_P3,
        KC_P0,            FR_DOT,  KC_PENT
    ),

    [_CALC] = LAYOUT(
        LR_CFUN, _______, _______, TD(TD_NUM),
        _______, _______, _______, _______,
        _______, _______, _______, _______,
        _______, _______, _______,
        _______, _______, _______,
        _______,          _______, _______
    ),

    [_CALC_FUNC] = LAYOUT(
        xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx,
        xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx,
        xxxxxxx, xxxxxxx, xxxxxxx, xxxxxxx,
        xxxxxxx, xxxxxxx, xxxxxxx,
        xxxxxxx, xxxxxxx, xxxxxxx,
        xxxxxxx,          xxxxxxx, SEND_RES
    ),

    [_FUNC] = LAYOUT(
        xxxxxxx, RGB_SAD, RGB_SAI, RGB_TOG,
        RGB_MOD, RGB_HUD, RGB_HUI, RGB_VAD,
        TIMER_7, TIMER_8, TIMER_9, RGB_VAI,
        TIMER_4, TIMER_5, TIMER_6,
        TIMER_1, TIMER_2, TIMER_3,
        KC_NUM,           KC_BSPC, START_CALC
    ),

};

bool numpad_or_calc_operator(keyrecord_t *record, const char input) {
    if (layer_state_is(_CALC)) {
        calc_operator(input);
        return false;
    }
    return true;
}

bool numpad_or_calc(keyrecord_t *record, const char input) {
    if (layer_state_is(_CALC)) {
        calc_append(input);
        return false;
    }
    return true;
}

bool timer_min(keyrecord_t *record, uint16_t keycode) {
    calc_write("==> timer");
    SEND_STRING( SS_DOWN( X_LGUI ) SS_TAP( X_SPACE ) SS_UP( X_LGUI ) );
    wait_ms( 200 );
    SEND_STRING("timer" SS_TAP( X_ENTER ) );
    wait_ms( 1000 );
    tap_code(KC_P1 + keycode - TIMER_1);
    SEND_STRING( SS_TAP( X_ENTER ) );
    calc_off();
    return true;
}

// RGB Backlight timeout feature
static uint16_t idle_timer = 0;
static uint8_t halfmin_counter = 0;

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    if (record->event.pressed) {
        if (!rgblight_is_enabled()) {
            rgblight_enable_noeeprom();
        }
        idle_timer = timer_read();
        halfmin_counter = 0;
    }

    if (keycode == LR_CFUN) {
        static uint16_t func_timer;
        // switch to layer _CALC_FUNC on hold, delete last char on tap
        if (record->event.pressed) {
            func_timer = timer_read();
            layer_on(_CALC_FUNC);
            return false;
        } else {
            layer_off(_CALC_FUNC);
            if (timer_elapsed(func_timer) < TAPPING_TERM) {
                calc_remove_last();
            }
        }
        return true;
    }

    if (record->event.pressed) {
        switch (keycode) {
            case KC_P0: return numpad_or_calc(record, '0');
            case KC_P1: return numpad_or_calc(record, '1');
            case KC_P2: return numpad_or_calc(record, '2');
            case KC_P3: return numpad_or_calc(record, '3');
            case KC_P4: return numpad_or_calc(record, '4');
            case KC_P5: return numpad_or_calc(record, '5');
            case KC_P6: return numpad_or_calc(record, '6');
            case KC_P7: return numpad_or_calc(record, '7');
            case KC_P8: return numpad_or_calc(record, '8');
            case KC_P9: return numpad_or_calc(record, '9');
            case FR_DOT: return numpad_or_calc(record, '.');
            case KC_PSLS: return numpad_or_calc_operator(record, '/');
            case KC_PAST: return numpad_or_calc_operator(record, '*');
            case KC_PMNS: return numpad_or_calc_operator(record, '-');
            case KC_PPLS: return numpad_or_calc_operator(record, '+');
            case FR_LPRN: return numpad_or_calc(record, '(');
            case FR_RPRN: return numpad_or_calc(record, ')');
            case KC_ESC:
                if (layer_state_is(_CALC)) {
                    calc_reset_input_and_screen(true);
                    return false;
                }
                break;
            case KC_PENT:
                if (layer_state_is(_CALC)) {
                    calc_append('=');
                    calc_calculate();
                    return false;
                }
                break;
            case TIMER_1 ... TIMER_9:
                return timer_min(record, keycode);
            case START_CALC:
                calc_write("==> calculator");
                SEND_STRING( SS_DOWN( X_LGUI ) SS_TAP( X_SPACE ) SS_UP( X_LGUI ) );
                wait_ms( 200 );
                SEND_STRING("calculator" SS_TAP( X_ENTER ) );
                calc_off();
                break;
            case SEND_RES:
                {
                    char* output = calc_get_output();
                    if (strlen(output) == 0) {
                        output = calc_get_input();
                    }
                    // send char manually instead of using send_string() with
                    // sendstring_french_os_x.h in order to work independantly
                    // with OS X, linux or windows
                    for (int i = 0; output[i] != '\0'; i++) {
                        char c = output[i];
                        if ( c >= '1' && c <= '9') {
                            tap_code(KC_P1 - 1 + (c - '0'));
                        } else if (c == '.') {
                            SEND_STRING(".");
                        } else if (c == '0') {
                            tap_code(KC_P0);
                        } else if (c == '/') {
                            tap_code(KC_PSLS);
                        } else if (c == '*') {
                            tap_code(KC_PAST);
                        } else if (c == '-') {
                            tap_code(KC_PMNS);
                        } else if (c == '+') {
                            tap_code(KC_PPLS);
                        } else if (c== '(') {
                            SEND_STRING("(");
                        } else if (c== ')') {
                            SEND_STRING(")");
                        }
                    }
                }
                break;
        }
    }
    // Let QMK process the keycode as usual
    return true;
}

void dance_num(qk_tap_dance_state_t *state, void *user_data) {
    if (state->count == 1) {
        if (layer_state_is(_CALC)) {
            layer_off(_CALC);
            calc_off();
        } else {
            layer_on(_CALC);
            calc_reset_input_and_screen(true);
        }
    } else if (state->count == 2) {
        if (!layer_state_is(_CALC)) {
            tap_code(KC_TAB);
        }
    } else if (state->count >= 4) {
        rgblight_sethsv_range(HSV_RED, 0, RGBLED_NUM);
        calc_write("    flashing...");
        reset_keyboard();
    }
}

qk_tap_dance_action_t tap_dance_actions[] = {
    [TD_NUM] = ACTION_TAP_DANCE_FN(dance_num)
};

void keyboard_post_init_user(void) {
    calc_init();
    // enables Rgb, without saving settings
    rgblight_enable_noeeprom();
    rgblight_mode_noeeprom(RGBLIGHT_DEFAULT_MODE);
    rgblight_sethsv_noeeprom(RGBLIGHT_DEFAULT_HUE, RGBLIGHT_DEFAULT_SAT, RGBLIGHT_DEFAULT_VAL);
    rgblight_set_speed_noeeprom(RGBLIGHT_DEFAULT_SPD);
}

void matrix_scan_user(void) {
    // idle_timer needs to be set one time
    if (idle_timer == 0) idle_timer = timer_read();

    if (rgblight_is_enabled()) {
        if (timer_elapsed(idle_timer) > 30000) {
            halfmin_counter++;
            idle_timer = timer_read();
        }

        if (halfmin_counter >= RGBLIGHT_TIMEOUT * 2) {
            rgblight_disable_noeeprom();
            calc_off();
            layer_off(_CALC);
            halfmin_counter = 0;
        }
    }
}
