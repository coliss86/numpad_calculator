/* Copyright 2021 Coliss86
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#ifdef __cplusplus
extern "C" {
#endif

void calc_init(void);
void calc_write(char *);
void calc_append(char);
void calc_remove_last(void);
void calc_operator(char);
void calc_calculate(void);
void calc_off(void);
void calc_reset_input_and_screen(bool);
char * calc_get_output(void);
char * calc_get_input(void);

#ifdef __cplusplus
}
#endif

void screen_display(char *value);
void double_to_str(double, char *);
