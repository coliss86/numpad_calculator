/* Copyright 2021 Coliss86
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "config_common.h"

/* USB Device descriptor parameter */
#define VENDOR_ID       0xBEAF
#define PRODUCT_ID      0x0999
#define DEVICE_VER      0x0001
#define MANUFACTURER    Coliss86
#define PRODUCT         Num pad calculator
#define DESCRIPTION     DYI num pad

/* key matrix size */
#define MATRIX_ROWS 6
#define MATRIX_COLS 4

/* key matrix pins */
// A11 & A12 are connected to the USB, so cannot be used
#define MATRIX_ROW_PINS { A8, A9, A10, A15, B3, B4 }
#define MATRIX_COL_PINS { B12, B13, B14, B15 }
#define UNUSED_PINS

/* COL2ROW or ROW2COL */
#define DIODE_DIRECTION COL2ROW

/* Set 0 if debouncing isn't needed */
#define DEBOUNCE 5

#define TAPPING_TERM 300

/* RGB light */
#define RGBLIGHT_ANIMATIONS
// Twinkle effect does not work

#define RGB_DI_PIN B5
#define RGBLED_NUM 13
#define RGBLIGHT_DEFAULT_HUE 80
#define RGBLIGHT_DEFAULT_SAT 255
#define RGBLIGHT_DEFAULT_VAL 120
#define RGBLIGHT_DEFAULT_SPD 200
#define RGBLIGHT_DEFAULT_MODE RGBLIGHT_MODE_RAINBOW_SWIRL
#define RGBLIGHT_TIMEOUT 10    // in minutes
