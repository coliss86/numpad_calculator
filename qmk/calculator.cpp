/*
 *  Infix Calculator
 *
 *  Copyright (C) 2021 Coliss86
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "calculator.h"
#include "infix_postfix.h"
#include "evaluate_postfix.h"
#include "string_utils.h"

uint8_t calculate(char *infix, double & numerical_result) {
  // check if the expression is empty.
  if (strlen(infix) >= EXPR_MAX_SIZE) {
    return ERR_TOO_LONG;
  }

  char postfix[EXPR_MAX_SIZE] = ""; // the postfix expression.
  // try to convert the infix expression to postfix.
  uint8_t status = infix_postfix(infix, postfix);
  if (status == NO_ERROR) {
    // try to evaluate the postfix expression.
    status = evaluate_postfix(postfix, numerical_result);
  }
  return status;
}
