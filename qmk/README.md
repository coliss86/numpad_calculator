# Flash Instructions

See the [build environment setup](https://docs.qmk.fm/#/getting_started_build_tools) and the [make instructions](https://docs.qmk.fm/#/getting_started_make_guide) for more information. Brand new to QMK? Start with our [Complete Newbs Guide](https://docs.qmk.fm/#/newbs).

## Import QMK firmware

```
git clone https://gitlab.com/coliss86/numpad_calculator
git clone -b 0.15.12 https://github.com/qmk/qmk_firmware
cd qmk_firmware/keyboards
ln -s ../../numpad_calculator/qmk numpad_calculator
```

To update `qmk_firmware`
```
cd qmk_firmware
git fetch -a
git checkout <new_tag>
```

## Flash the STM32

To build the firmware, launch the following command in the root of qmk folder after setting up your build environment :

```
qmk compile -kb numpad_calculator -km default
```

To flash, put the microcontroller in bootloader mode and launch :

```
qmk flash -kb numpad_calculator -km default
```

See the [build environment setup](https://docs.qmk.fm/#/getting_started_build_tools) and the [make instructions](https://docs.qmk.fm/#/getting_started_make_guide) for more information. Brand new to QMK? Start with our [Complete Newbs Guide](https://docs.qmk.fm/#/newbs).
