/*
 *  This file defines a postfix evaluation algorithm.
 *
 *  Copyright (C) 2021 Coliss86
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <stdint.h>

// postfix expression evaluation algorithm.
uint8_t evaluate_postfix(char *postfix, double & numerical_result);

bool postfix_push(double item);
double postfix_pop();
double postfix_peek();
bool postfix_empty();
int postfix_count();
void postfix_reset();
