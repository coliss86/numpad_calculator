/*
 *  Infix Calculator
 *
 *  Copyright (C) 2021 Coliss86
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <stdint.h>

#define EXPR_MAX_SIZE 70 // the maximum size of the infix in chars.
#define RESULT_MAX_SIZE 64 // the maximum result size in chars.

#define NO_ERROR 0
#define ERR_TOO_LONG 1
#define ERR_EXPR_EMPTY 2
#define ERR_INSUFFICIENT_OPERATOR 3
#define ERR_MEMORY 4
#define ERR_DIVISION_BY_0 5
#define ERR_UNKNOW_CHAR 6
#define ERR_EXPR_TOO_MANY_VALUES 7
#define ERR_RESULT_TOO_HIGH 8
#define ERR_STACKOVERFLOW 9
#define ERR_PARENTHESIS 10

uint8_t calculate(char *infix, double & numerical_result);
