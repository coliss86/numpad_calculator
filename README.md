# Numpad calculator ⌨️

A keyboard numpad and calculator combined into one device.

![Front](doc/front.jpg)

By default, it functions as a numpad and sends input to the host, and pressing the <kbd>Calc</kbd> key (upper right key) activates the calculator function which displays operations and results on the embedded VFD screen.
It uses the [Shunting-yard algorithm](https://en.wikipedia.org/wiki/Shunting-yard_algorithm) to convert expression written in [infix notation](https://en.wikipedia.org/wiki/Infix_notation) (e.g. `1+2*3`) to [postfix notation (also known as Reverse Polish notation or RPN)](https://en.wikipedia.org/wiki/Reverse_Polish_notation) (e.g. `1 2 3 * +`) in order to calculate the result of an expression.

This calculator supports the following operators:
- `+`, `-`, `*`, and `/` with `*` and `/` having precedence
- `(` and `)`

It also has a RGB underglow.

![Back](doc/back.jpg)

### Project structure

* **/case** - `stl`, `step` and `fusion 360` files
* **/qmk** - Firmware folder for QMK
* **/doc** - Pictures, Json files used for [keyboard-layout-editor.com](http://www.keyboard-layout-editor.com/)

## Layouts 👗👔

See [the keymap for full details](qmk/keymaps/default/keymap.c).

* base layer, default when starts, act as a numpad or as a calculator: ![Base Layout](doc/keyboard-layout-base.png)
* function layer: ![Func Layout](doc/keyboard-layout-func.png)

## Electronic & PCB 📟

I use Altana PCB: it is a [Kailh socket mounted on a PCB with per key underglow led](https://github.com/swanmatch/MxLEDBitPCB/tree/master/altana)

![Altana PCB](doc/pcb.jpg)

> ❗️ Pay attention to the name of the LED SK6812 MINI: the **-e** version is the same form factor but the pinout is not compatible.

The Altena PCB is designed for *SK6812 mini*, but I mistakenly ordered 100 SK6812 mini-e instead. When building this macro pad, I wanted underglow rather than per key leds. I compared the pinout of the *SK6812 mini* and *SK6812 mini-e* and found that if I invert *VCC* and *GND*, I can solder the *mini-e* facing the bottom of the keyboard and have my underglow effect, while keeping the *Data In* (`DI`) and *Data Out* `DO` pinouts unchanged.

The display I used is an old VFD (vacuum fluorescent display) with 2x20 characters wired using the SPI protocol. It gives a very nice appearance in both bright and low light conditions.

## 3D Case 🔧

Stl files are available in [the case folder](case/):
- [ ] 1 * Front
- [ ] 1 * Up
- [ ] 1 * Back

I used translucent PLA for the shell and bling PLA for the plate.

![Inside](doc/3dprint.jpg)

The STM32 is secured by an ergo on the top and also the left angle USB adapter by 2 other ergos.

![Inside](doc/inside_stm32.jpg)

## Software Dependencies 📦

- [QMK Firmware](https://github.com/qmk/qmk_firmware)
- [infix_calculator](https://gitlab.com/coliss86/infix_calculator) v2.0.1
- [SPI VFD ported in C](https://gitlab.com/coliss86/spi_vfd_c) v1.0.0
- [dtostrf from stm32duino](https://github.com/stm32duino/Arduino_Core_STM32/blob/master/cores/arduino/avr/dtostrf.c)

### Bill of material 🧾

- [ ] 1 * STM32F103CT6 (aka Blue Pill)
- [ ] 1 * Left angle micro USB to type C
- [ ] 1 * 20T202DA2JA SPI VFD (vacuum fluorescent display)
- [ ] 21 * Cherry MX Switchs + Keycaps
- [ ] 21 * Altana PCB: [Kailh socket mounted on a PCB per key with underglow led](https://github.com/swanmatch/MxLEDBitPCB/tree/master/altana).
- [ ] 8 * LED SK6812 MINI-**E**
- [ ] 21 * Diode 1N4148
- [ ] 2 * M3x30
- [ ] 2 * M3x22
- [ ] 4 * M3x16
- [ ] Wire (I salvaged a round IDE cable)
- [ ] Dupont connector
- [ ] Optionnal: magnetic USB-C connector + cable
